using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CanvasRenderer))]
public class ProgressBar : MaskableGraphic {
    public float progress = 0.0f;

    public Color color;

    protected override void OnPopulateMesh(VertexHelper vh) {
        vh.Clear();

        float w = rectTransform.rect.width;
        float h = rectTransform.rect.height;

        UIVertex vertex = UIVertex.simpleVert;
        vertex.color = color;

        float perc0 = 0.0f;
        float perc1 = progress * w;

        vertex.position = new Vector3(perc0 + rectTransform.pivot.x, rectTransform.pivot.y, 0.0f);
        vh.AddVert(vertex);
        vertex.position = new Vector3(perc1 + rectTransform.pivot.x, rectTransform.pivot.y, 0.0f);
        vh.AddVert(vertex);
        vertex.position = new Vector3(perc1 + rectTransform.pivot.x, rectTransform.pivot.y + h, 0.0f);
        vh.AddVert(vertex);
        vertex.position = new Vector3(perc0 + rectTransform.pivot.x, rectTransform.pivot.y + h, 0.0f);
        vh.AddVert(vertex);

        vh.AddTriangle(0, 1, 2);
        vh.AddTriangle(0, 2, 3);
    }
}
