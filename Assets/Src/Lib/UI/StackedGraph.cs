using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CanvasRenderer))]
public class StackedGraph : MaskableGraphic {
    public float lineWidth = 2.0f;

    public List<float> percentages = new List<float> { 1.0f };

    public List<Color> colors = new List<Color> { Color.white };

    public void SetData(List<float> percentages, List<Color> colors) {
        this.percentages = percentages;
        this.colors = colors;
    }

    protected override void OnPopulateMesh(VertexHelper vh) {
        vh.Clear();

        float w = rectTransform.rect.width;
        float h = rectTransform.rect.height;

        UIVertex vertex = UIVertex.simpleVert;
        for (int i = 0; i < percentages.Count; ++i) {
            vertex.color = colors[i];

            float perc0 = w * (i == 0 ? 0.0f : percentages[i - 1]);
            float perc1 = w * percentages[i];

            vertex.position = new Vector3(perc0 + rectTransform.pivot.x, rectTransform.pivot.y, 0.0f);
            vh.AddVert(vertex);
            vertex.position = new Vector3(perc1 + rectTransform.pivot.x, rectTransform.pivot.y, 0.0f);
            vh.AddVert(vertex);
            vertex.position = new Vector3(perc1 + rectTransform.pivot.x, rectTransform.pivot.y + h, 0.0f);
            vh.AddVert(vertex);
            vertex.position = new Vector3(perc0 + rectTransform.pivot.x, rectTransform.pivot.y + h, 0.0f);
            vh.AddVert(vertex);

            vh.AddTriangle(4 * i + 0, 4 * i + 1, 4 * i + 2);
            vh.AddTriangle(4 * i + 0, 4 * i + 2, 4 * i + 3);
        }
    }
}
