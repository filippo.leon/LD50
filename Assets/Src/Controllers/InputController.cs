using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour {
    public World world;

    public int vaccineCharges = 100;
    
    public int cureCharges = 100;

    public float nukeCooldown = 10.0f;

    public float nukeCooldownMax = 10.0f;

    void Update() {
        if (!world.started) {
            return;
        }

        nukeCooldown -= Time.deltaTime;

        if (Input.GetMouseButtonDown(0)) { // TODO cooldown
            Vector3 newPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hitInfo = Physics2D.Raycast(newPos, Vector2.zero);
            if (hitInfo) {
                Cell cell = Util.GetCell(newPos);
                if (cell.mode == Cell.Mode.White) {
                    cell.SetMode(Cell.Mode.Immune);
                    vaccineCharges--;
                }
            }
        }

        if (Input.GetMouseButtonDown(2)) { // TODO cooldown
            Vector3 newPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hitInfo = Physics2D.Raycast(newPos, Vector2.zero);
            if (hitInfo) {
                Cell cell = Util.GetCell(newPos);
                if (cell.mode == Cell.Mode.Infected && Random.Range(0.0f, 1.0f) < 0.5f) {
                    cell.SetMode(Cell.Mode.Healed);
                    cureCharges--;
                }
            }
        }

        if (Input.GetMouseButtonDown(1) && nukeCooldown < 0.0f) {
            Vector3 newPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hitInfo = Physics2D.Raycast(newPos, Vector2.zero);
            if (hitInfo) {
                nukeCooldown = nukeCooldownMax;
                int range = 3;
                Vector2Int size = new Vector2Int(range + 1, range + 1);
                for (int i = -size.x; i <= size.x; ++i) {
                    for (int j = -size.y; j <= size.y; ++j) {
                        Vector3 dist = new Vector3(i, j, 0.0f);
                        if (dist.magnitude < range) {
                            Cell cell = Util.GetCell(newPos + dist);
                            if (cell) {
                                cell.SetMode(Cell.Mode.Destroyed);
                            }
                        }
                    }
                }
            }
        }
    }
}
