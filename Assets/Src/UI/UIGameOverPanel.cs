using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIGameOverPanel : MonoBehaviour {
    public World world;

    public Text gameOverText;

    void Update() {
        if (world.started 
                && world.mode2count[(int)Cell.Mode.Invalid] == 0 
                && world.mode2count[(int)Cell.Mode.Infected] == 0 
                && world.mode2count[(int)Cell.Mode.SuperInfected] == 0) {
            int score = 2 * world.mode2count[(int)Cell.Mode.White] + world.mode2count[(int)Cell.Mode.Healed];
            gameOverText.text = "Game Over!\nScore: " + score;
            gameOverText.gameObject.SetActive(true);
        }
    }
}
