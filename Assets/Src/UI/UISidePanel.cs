using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISidePanel : MonoBehaviour {
    public World world;

    public InputController inputController;

    public ProgressBar nukeProgressBar;

    void Update() {
        nukeProgressBar.progress = Mathf.Clamp(inputController.nukeCooldown, 0.0f, inputController.nukeCooldownMax) 
            / inputController.nukeCooldownMax;
        nukeProgressBar.SetAllDirty();
    }
}
