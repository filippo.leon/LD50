using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIMainPanel : MonoBehaviour {
    public World world;

    public InputController inputController;
    
    public StackedGraph stackedGraph;

    void Update() {
        List<float> percentages = new List<float> { };

        List<Color> colors = new List<Color> { };
        int total = 0;
        for (int i = 0; i < world.mode2count.Count; ++i) {
            int m2c = world.mode2count[i];
            if ((Cell.Mode)i != Cell.Mode.Invalid) {
                total += m2c;
            }
        }
        float pos = 0.0f;
        for (int i = 0; i < world.mode2count.Count; ++i) {
            if ((Cell.Mode)i != Cell.Mode.Invalid) {
                int m2c = world.mode2count[i];
                pos += m2c / (float)total;
                percentages.Add(pos);
                colors.Add(world.GetColor((Cell.Mode)i));
            }
        }

        stackedGraph.SetData(percentages, colors);
        stackedGraph.SetAllDirty();
    }
}
