using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell : MonoBehaviour {
    public enum Mode { Invalid, White, Infected, SuperInfected, Healed, Immune, Destroyed }

    public Mode mode = Mode.Invalid;

    public World world;

    private Mode lastMode = Mode.Invalid;

    private SpriteRenderer spriteRenderer;

    public void SetMode(Mode mode) {
        if (mode == lastMode) {
            return;
        }
        world.ChangeMode(lastMode, mode);

        lastMode = mode;
        this.mode = mode;
        spriteRenderer.color = world.GetColor(mode);

    }

    void Start() {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    void FixedUpdate() {
        SetMode(mode); // If somebody changes mode without using mode

        switch (mode) {
            case Mode.White:
                if (Random.Range(0.0f, 1.0f) < world.RandomWhiteInfectionProbability) {
                    SetMode(Mode.Infected);
                }
                break;
            case Mode.Infected:
            case Mode.SuperInfected:
                float multiplier = mode == Mode.SuperInfected ? 2.0f : 1.0f;
                if (Random.Range(0.0f, 1.0f) < world.RandomInfectedSpreadProbability * multiplier) {
                    Cell cell = Util.GetCell(transform.position, Random.Range(0, 4));
                    if (cell && (cell.mode == Mode.White || 
                            ((cell.mode == Mode.Healed || cell.mode == Mode.Immune) 
                                && Random.Range(0.0f, 1.0f) < world.RandomInfectedSpreadToImmuneProbability * multiplier))) {
                        cell.SetMode(Random.Range(0.0f, 1.0f) < world.SuperInfectedProbability * multiplier ? Mode.SuperInfected : Mode.Infected);
                    }
                }
                if (Random.Range(0.0f, 1.0f) < world.RandomInfectedHealingProbability) {
                    SetMode(Mode.Healed);
                } else if (Random.Range(0.0f, 1.0f) < world.RandomInfectedDeathProbability) {
                    SetMode(Mode.Destroyed);
                }
                break;
        }
    }
}
