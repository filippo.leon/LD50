using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generator : MonoBehaviour {
    public Vector2Int size;

    public GameObject cellPrefab;

    void Start() {
        // Init();
    }

    public void Init() {

        World world = GetComponent<World>();
        world.size = size;

        for (int i = 0; i < size.x; ++i) {
            for (int j = 0; j < size.y; ++j) {
                Vector3 position = new Vector3(i - size.x / 2, j - size.y / 2, 0.0f);
                GameObject go = Instantiate(cellPrefab, position, new Quaternion());
                go.transform.SetParent(transform);
                Cell cell = go.GetComponent<Cell>();
                cell.world = world;
                cell.mode = Cell.Mode.White;
            }
        }

        int ir = Random.Range(0, size.x);
        int jr = Random.Range(0, size.y);
        Cell cellr = Util.GetCell(new Vector3(ir - size.x / 2, jr - size.y / 2, 0.0f));
        cellr.mode = Cell.Mode.Infected;

        world.started = true;
    }

}
