using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class World : MonoBehaviour {
    // Dictionary<Vec2Int, GameObject> pos2cell = new Dictionary<Vec2Int, GameObject>();

    // public void Add(Vec2Int pos, GameObject go) {

    // }

    // public void Add(Vec2Int pos, GameObject go) {

    // }
    public bool started = false;
    
    public Vector2Int size;

    public float randomWhiteInfectionProbability = 0.00001f;

    public float randomInfectedSpreadProbability = 0.01f;

    public float randomInfectedSpreadToImmuneProbability = 0.01f;

    public float randomInfectedHealingProbability = 0.001f;

    public float randomInfectedDeathProbability = 0.001f;

    public float superInfectedProbability = 0.5f;

    public float randomWhiteInfectionProbabilityScale = 1.0f;

    public float randomInfectedSpreadProbabilityScale = 1.0f;

    public float randomInfectedSpreadToImmuneProbabilityScale = 1.0f;

    public float randomInfectedHealingProbabilityScale = 1.0f;

    public float randomInfectedDeathProbabilityScale = 1.0f;

    public float superInfectedProbabilityScale = 1.0f;

    public float RandomWhiteInfectionProbability {
        get { return randomWhiteInfectionProbability * randomWhiteInfectionProbabilityScale; }
    }

    public float RandomInfectedSpreadProbability {
        get { return randomInfectedSpreadProbability * randomInfectedSpreadProbabilityScale; }
    }

    public float RandomInfectedSpreadToImmuneProbability {
        get { return randomInfectedSpreadToImmuneProbability * randomInfectedSpreadToImmuneProbabilityScale; }
    }

    public float RandomInfectedHealingProbability {
        get { return randomInfectedHealingProbability * randomInfectedHealingProbabilityScale; }
    }

    public float RandomInfectedDeathProbability {
        get { return randomInfectedDeathProbability * randomInfectedDeathProbabilityScale; }
    }

    public float SuperInfectedProbability {
        get { return superInfectedProbability * superInfectedProbabilityScale; }
    }

    public List<int> mode2count = new List<int>();

    public Color normalColor;

    public Color infectedColor;

    public Color superInfectedColor;

    public Color destroyedColor;

    public Color healedColor;
    
    public Color immuneColor;

    public Color GetColor(Cell.Mode mode) {
        switch (mode) {
            case Cell.Mode.White:
                return normalColor;
            case Cell.Mode.Infected:
                return infectedColor;
            case Cell.Mode.SuperInfected:
                return superInfectedColor;
            case Cell.Mode.Destroyed:
                return destroyedColor;
            case Cell.Mode.Healed:
                return healedColor;
            case Cell.Mode.Immune:
                return immuneColor;
        }

        return new Color32(0, 0, 0, 255);
    }

    public void SetSize(String i) {
        size.x = 40;
    }

    public void ChangeMode(Cell.Mode from, Cell.Mode to) {
        mode2count[(int)from]--;
        mode2count[(int)to]++;
    }

    void Start() {
        for (int i = 0; i < (int)Cell.Mode.Destroyed + 1; ++i) {
            mode2count.Add(0);
        }
        mode2count[(int)Cell.Mode.Invalid] = size.x * size.y;
        mode2count[(int)Cell.Mode.White] = 0;
        mode2count[(int)Cell.Mode.Infected] = 0;
        mode2count[(int)Cell.Mode.SuperInfected] = 0;
        mode2count[(int)Cell.Mode.Healed] = 0;
        mode2count[(int)Cell.Mode.Immune] = 0;
        mode2count[(int)Cell.Mode.Destroyed] = 0;
    }

    void Update() {
        // foreach (var m2c in mode2count) {
        //     Debug.LogFormat("{0} {1}", m2c.Key, m2c.Value);
        // }
    }
}
