
using UnityEngine;

public class Util {

    private static Vector3[] dirs = new Vector3[4] {
        Vector3.up, Vector3.left, Vector3.down, Vector3.right
    };

    public static Cell GetCell(Vector3 pos, int dir) {
        Vector3 newPos = pos + dirs[dir];
        RaycastHit2D hitInfo = Physics2D.Raycast(newPos, Vector2.zero);
        if (hitInfo) {
            return hitInfo.transform.gameObject.GetComponent<Cell>(); ;
        }
        return null;
    }

    public static Cell GetCell(Vector3 pos) {
        RaycastHit2D hitInfo = Physics2D.Raycast(pos, Vector2.zero);
        if (hitInfo) {
            return hitInfo.transform.gameObject.GetComponent<Cell>();
        }
        return null;
    }
}
